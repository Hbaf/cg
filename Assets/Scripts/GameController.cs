﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    private readonly int warriorAmount = 5;
    private readonly int enemyAmount = 5;

    private readonly float spawnTimer = 7f;
    private float nextSpawn = 0;

    private float maxTop = -15;
    private float maxBottom = 15;


    [SerializeField] private Transform warrior;
    [SerializeField] private Transform enemy;

    void Start()
    {
        SpawnUnits();
    }

    void Update()
    {
        if (Time.time >= nextSpawn)
        {
            SpawnUnits();
        }
    }

    void SpawnUnits()
    {
        for (int i = 0; i < warriorAmount; i++)
        {
            Instantiate(warrior, new Vector3(-10, Random.Range(maxTop, maxBottom)), Quaternion.identity);
        }

        for (int i = 0; i < enemyAmount; i++)
        {
            Instantiate(enemy, new Vector3(+10, Random.Range(maxTop, maxBottom)), Quaternion.identity);
        }

        nextSpawn = Time.time + spawnTimer;
    }
}
