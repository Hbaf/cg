﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warrior : MonoBehaviour
{
    public Rigidbody2D rb;
    public Animator animator;
    private Vector2 moveInput;

    [SerializeField] private bool isEnemy;
    [SerializeField] private GameObject blood;
    
    private readonly int maxHealth = 100;
    private int currentHealth;

    private readonly int damageAmount = 20;
    private readonly float attackDistance = 1f;
    private readonly float attackRate = 1f;
    private float nextAttackTime = 0;
    
    private bool isAlive = true;
    
    private readonly float moveSpeed = 3f;

    private float timeToLive = 5f;

    private Warrior target = null;


    private static List<Warrior> warriorList = new List<Warrior>();

    private static Warrior GetClosest(bool isEnemy, Vector3 position)
    {
        Warrior closest = null;

        foreach (Warrior warrior in warriorList)
        {
            if (warrior.isEnemy == isEnemy)
            {
                if (closest == null)
                {
                    closest = warrior;
                } else
                {
                    if (Vector3.Distance(warrior.GetPosition(), position) < Vector3.Distance(closest.GetPosition(), position))
                    {
                        closest = warrior;
                    }
                }
            }
        }
        return closest;
    }

    void Start()
    {
        animator.SetFloat("Speed", moveSpeed);
        currentHealth = maxHealth;
        animator.SetBool("Normal", true);
        warriorList.Add(this);
        
    }

    public void TakeDamage(int damage)
    {
        Instantiate(blood, GetPosition(), Quaternion.identity);
        currentHealth -= damage;

        if (currentHealth <= 0)
        {
            Die();
        }
    }


    void FixedUpdate()
    {
        if (isAlive)
        {


            target = GetClosest(!isEnemy, GetPosition());
            HandleMovement();

            HandleAttack();
        } else
        {
            timeToLive -= Time.deltaTime;
            if (timeToLive <= 0)
            {
                Destruction();
            }
        }
    }

    void HandleMovement()
    {
        if (target != null)
        {
            animator.SetFloat("Speed", moveSpeed);
            if ((Vector3.Distance(GetPosition(), target.GetPosition()) > attackDistance))
            {
                transform.position = Vector2.MoveTowards(GetPosition(), target.GetPosition(), moveSpeed * Time.fixedDeltaTime);
                animator.SetFloat("Horizontal", (target.GetPosition() - GetPosition()).normalized[0]);
            }
        }
        else
        {
            animator.SetFloat("Speed", 0);
        }
    }

    void HandleAttack()
    {

        if (Time.time >= nextAttackTime && target != null)
        {
            if (Vector3.Distance(GetPosition(), target.GetPosition()) <= attackDistance)
            {
                // Attack
                animator.SetTrigger("Attack");
                target.TakeDamage(damageAmount);
                nextAttackTime = Time.time + 1f / attackRate;
            }
        }
    }

    private void Die()
    {
        isAlive = false;
        animator.SetBool("IsDead", true);
        GetComponent<Collider2D>().enabled = false;
        animator.SetFloat("Speed", 0);
        warriorList.Remove(this);
        
    }

    private void Destruction()
    {
        Destroy(this.gameObject);
        this.enabled = false;
    }

    private Vector3 GetPosition()
    {
        return transform.position;
    }
}
